/* 

## Задание

Написать реализацию кнопки "Показать пароль". 
Задача должна быть реализована на языке javascript, 
без использования фреймворков и сторонник библиотек (типа Jquery).

#### Технические требования:
- В файле `index.html` лежит разметка для двух полей ввода пароля.

- По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, 
    иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, 
    именно она должна отображаться вместо текущей.

- Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль).

- Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль).

- По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях.

- Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;

- Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые значения`.

- После нажатия на кнопку страница не должна перезагружаться.

- Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

*/
const fieldEnter = document.getElementById('input1');
const showPasswordIconEnter = document.getElementById('hide1');
const hidePasswordIconEnter = document.getElementById('hide2');

const fieldConfirm = document.getElementById('input2');
const showPasswordIconConfirm = document.getElementById('hide3');
const hidePasswordIconConfirm = document.getElementById('hide4');

const loginBtn = document.querySelector('.login-btn');

function showPassword(type) {

    if (type === 'Enter') {
        toggleIcon(fieldEnter, showPasswordIconEnter, hidePasswordIconEnter)
    } else if (type === 'Confirm') {
        toggleIcon(fieldConfirm, showPasswordIconConfirm, hidePasswordIconConfirm)
    }
};

function toggleIcon(enterField, showPasswordIcon, hidePasswordIcon) {
    if (enterField.type === 'password') {
        enterField.type = 'text';
        showPasswordIcon.style.display = 'block';
        hidePasswordIcon.style.display = 'none';
    } else {
        enterField.type = 'password';
        showPasswordIcon.style.display = 'none';
        hidePasswordIcon.style.display = 'block';
    }
};

loginBtn.addEventListener('click', (event) => {
    event.preventDefault();
    const error = document.querySelector('.error')

    if (error) {
        error.remove();
    }

    if (fieldEnter.value === fieldConfirm.value) {
        setTimeout(() => alert('You are welcome'), 500)
        
    } else {
        const error = document.createElement('span');
        error.classList.add('error');
        error.innerText = 'Enter correct password';
        fieldConfirm.after(error);
    }
});
